Role Name
=========
- This role creates backups for the VMs listed in the input variable (`vms_list`) according to the specified backup type

**Note:** Currently the only supported backup type is `snapshot`, which will launch cronjobs on control plane hypervisors
for periodically creating VM snapshots.

Requirements
------------
- `docker` version >= 1.13.1 installed on control plane hypervisors
- `rhosp13/openstack-nova-libvirt:13.0-149` or newer deployed on top of docker
- `cronie` version >= 1.4.11  installed on control plane hypervisors

Role Variables
--------------
- `vms_list` : `<list>` - the list of VMs for which backup is desired

   Each item in the `vms_list` should be a dictionary containing the following keys:
     - `vm_name` : `<string>` - the hostname of the VM that should be backed up (e.g. m1-idm0002.mgmt.oiaas)
     - `cron_schedule` : `<dict>` - a dictionary containing information about when the cronjob should be executed
     - `backup_type` : `<string>` - a string representing what type of backup is desired (only 'snapshot' supported for now)
  
  Here is an example for creating snapshots on IDM 2 VM every sunday midnight:
  ```yaml
  vms_list:
  - vm_name: m1-idm0002.mgmt.oiaas
    cron_schedule:
      weekday: 0
      hour: 0
    backup_type: snapshot
  ```

Example Playbook
----------------

```python
---
- name: launch backup scripts
  hosts: localhost
  roles:
    - role: backup
      vms_list: "{{ backup_list }}"
  tags:
    - backup
```
